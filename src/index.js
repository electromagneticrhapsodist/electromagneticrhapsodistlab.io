const fs = require("fs");
const config = require("./config");
const postMethods = require("./posts");
const jobMethods = require("./jobs");
const eduMethods = require("./education")

const homepage = jobs => `
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="${config.homePageDescription}" />
        <title>${config.homePageName}</title>
    </head>
    <body>
        <div class="grotesk">
            <header>
                <h1>${config.homePageName}</h1>
                <p>—</p>
                <a href="mailto:${config.email}"/>Open to Work - Email Me</a>
                <p>Resume for ${config.authorName}, ${
  config.authorDescription
} </p>
<a href="${config.linkedIn}">Contact Me on LinkedIn</a>
                <hr />
            </header>
            <h2>Experience</h2>
            <div class="jobs">
                ${jobs
                  .map(
                    job => `<div class="job">
                    <h3>${job.attributes.title}</h3>
                                ${job.body}
                            </div>
                        </div>`)
                  .join("")}
            </div>
<!--            <div class="posts">
                 ${posts
                   .map(
                     post => `<div class="post">
    <h4>${post.attributes.title}</h4>
    ${post.body}
</div>`).join("")}
            </div>     -->
            <hr />
            <h2>Education</h2>
            <div class="education">
                 ${edus
        .map(
            edus => `<div class="edus">
    <h4>${edus.attributes.title}</h4>
    ${edus.body}
</div>`).join("")}
            </div>
            <footer>
                ${`<p>© ${new Date().getFullYear()} ${
                  config.authorName
                }, </p>`}
            </footer>
        </div>
    </body>
</html>
`;

const addHomePage = jobs => {
  fs.writeFile(`${config.dev.outdir}/index.html`, homepage(jobs), e => {
    if (e) throw e;
    console.log(`index.html was created successfully`);
  });
};

const jobs = fs
      .readdirSync(config.dev.jobsdir)
      .map(job => jobMethods.createJob(job))
      .sort(function(a, b) {
	  return b.attributes.order - a.attributes.order;
      });

const posts = fs
      .readdirSync(config.dev.postsdir)
      .map(post => postMethods.createPost(post))
      .sort(function(a, b) {
	  return b.attributes.order - a.attributes.order;
      });

const edus = fs
    .readdirSync(config.dev.edudir)
    .map(edu => eduMethods.createEdu(edu))
    .sort(function (a, b) {
        return b.attributes.order - a.attributes.order;
    });

if (!fs.existsSync(config.dev.outdir)) fs.mkdirSync(config.dev.outdir);

eduMethods.createEdus(edus);
postMethods.createPosts(posts);
addHomePage(jobs);

