
const config = {
  homePageName: "C. Copeland Resume",
  homePageDescription: "C. Copeland Resume Homepage",
  authorName: "C. Copeland",
  linkedIn: "https://www.linkedin.com/in/cacopela/",
  email: "cacopeland@protonmail.com",
  authorDescription:
    "Engineering Systems of Systems Architect, Scrum Master, Agilist and Continuous Delivery Evangelist with over 20 years experience across Commercial and DoD, Satellite Systems, eCommerce and FinTech.",
  dev: {
      jobsdir: "./content/jobs",
      postsdir: "./content/author/blog",
      edudir: "./content/education",
      paperdir: "./content/author/paper",
      outdir: "./public"
  }
};

module.exports = config;
