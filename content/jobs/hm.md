---
title: Happy Money
date: "February 2, 2025"
description: Latest Employment
company: Happy Money
role: Staff Engineer
start_date: "June 2022"
end_date: "Present"
order: "6"
---

_Staff Engineer_ : _June 2022 - June 2024_
Experienced Scrum Master and technical lead with expertise managing globally diverse teams in 24x7 full-stack development, operations, and support. Proficient in integrating systems across AWS EC2, Java Spring Boot, Kafka, DynamoDB, DataDog, Ansible, and Salesforce. 

Key Achievements
- Agile Leadership: Developed a lightweight project management framework (A2C2: Alignment, Accountability, Cadence, Calibration) based on SAFe principles, optimized for small teams to ensure efficiency and adaptability. 
- Streamlined Operations: Migrated manual and semi-automated pipelines to fully automated systems, reducing defects and downtime by 34%. Secured global handoff of final engineering checks, enabling continuous "follow-the-sun" development and support. 
- Team Management: Guided teams through leadership transitions and resource constraints using the Tuckman model. Created onboarding guides and generalized practices, enabling new members to contribute within a single sprint. 
- Process Optimization: Leveraged Jira, Confluence, and Atlassian tools to decompose value streams into user stories, system diagrams, and workflows aligned with business goals. 

Project Highlights 
- Lead Generation Funnel: Designed a new user experience and backend services using Kafka and Spring Boot while maintaining legacy loan origination operations. 
- Identity Management: Developed a "person" service to manage identity relationships across loan workflows. 
- Messaging System: Created an SMS service integrated with Salesforce CRM and internal platforms for seamless communication. 
- Strategic Planning: Conducted trade studies and prioritized competing initiatives using Weighted Shortest Job First (WSJF) to balance tactical gains and strategic growth. 

Servant Leadership 
Empowered teams with structured workflows, sprint alignment, and periodic reviews. Delivered results under challenging conditions by fostering collaboration and balancing strategic and tactical priorities.

Skills: Ansible · Software Industry · Elasticsearch · Messaging · Amazon Web Services (AWS) · Spring Boot · Domain Architecture · DevOps · Apache Kafka · Attention to Detail · Code Review · Systems Management · Data Centers
