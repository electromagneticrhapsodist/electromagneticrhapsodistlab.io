---
title: Intech
date: "February 2, 2025"
description: Intech Employment
company: Intech
role: Contractor
start_date: "April 2022"
end_date: "June 2022"
order: "5"
---

_Contractor/Consultant_ : _April 2022 - June 2022_
Agile/Scrum consultant supporting Hughes Aerospace's development of ground-based systems for bandwidth scheduling across Wideband Global SATCOM (WGS) and commercial satellites. Expertise in secure data transport, behavioral-driven design, and continuous delivery pipelines. 

Key Achievements 
- Behavioral-Driven Development: Designed and implemented Given-When-Then (GWT) frameworks using Cucumber, integrated with Jenkins and Maven on the Unclassified Government Cloud (AWS). 
- Secure Data Transport: Developed hybrid data privacy models to ensure secure transfer of user and system metadata between on-premise and multi-cloud environments. 
- Agile Mentorship: Guided junior developers in Agile/Scrum methodologies, emphasizing sprint cycles and in-office Agile ceremonies, including customer demonstrations. 

Project Highlights 
- Scheduling Systems: Implemented Java Spring solutions for Protected Tactical Waveform scheduling and bandwidth management. 
- Continuous Delivery: Designed a path forward for fully automated CI/CD pipelines. Secured customer and project manager buy-in by demonstrating increased developer productivity and reduced late-breaking software defects. 

Collaborative Leadership 
Fostered collaboration through a hybrid office schedule, ensuring sprint alignment and successful customer-facing Agile ceremonies. Delivered secure, scalable solutions aligned with project objectives and customer needs.

Skills: DevOps · Attention to Detail · Code Review · Software Architecture · Java Development · Integrated Systems · 
Domain Architecture

[PTES]: https://www.hughes.com/resources/press-releases/hughes-awarded-contract-boeing-develop-protected-tactical-enterprise
