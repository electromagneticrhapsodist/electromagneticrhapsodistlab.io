---
title: General Dynamics Mission Systems
date: "Februay 2, 2025"
description: General Dynamics Employment
company: General Dynamics
role: Advanced Spacecraft Systems Architect
start_date: "June 2016"
end_date: "December 2019"
order: "3"
---

_Systems Engineer - Advanced Spacecraft_ : _June 2016 - December 2019_
Conducted comprehensive reviews and provided technical leadership to improve efficiency, ensure timely delivery, and advance next-generation classified payloads for satellite systems. 

Systems Review and Improvement 
- Reviewed four payload systems across the systems V lifecycle, identifying bottlenecks and recommending test and verification enhancements to improve efficiency and reduce rework. 
- Delivered actionable reports to the prime customer, enabling on-time payload delivery with improved system performance. 
- Provided direct support for interferometry (PLG) and polarimetry (PLD) payload subsystems, overseeing acceptance and delivery to customers. 

Proposal and Ground System Expertise
- Served as Ground System SME for three fast-track proposals, defining CONOPS and schedules for next-generation classified small-satellite opportunities. 
- Delivered robust technical and operational frameworks to support advanced classified missions. 

Leadership in Algorithm Development and System Integration 
- Joined a six-month contract mid-term, leading a small team to mature lossless compression algorithms from TRL 4 to TRL 7. 
- Spearheaded system integration and testing, transitioning software from MATLAB to C++ and deploying in a classified environment with live data. 
- Restructured algorithm input formats to handle a 10x data volume increase, improving performance and optimizing memory/storage. 
- Successfully turned around a failing mid-term review by showcasing improved algorithm performance and bi-weekly proof-of-concept reports. 
- Delivered a successful contract with fee awards, securing a follow-on contract that increased revenue by 50%. 

Key Accomplishments 
- Supported final on-orbit verification and validation for an AIS payload as one of two SMEs, ensuring mission success post-launch. 
- Led systems engineering efforts for an interferometry payload, managing requirements, design, and manufacturing through detailed reviews.

Skills: Modernization · Legacy Modernization · Amazon Web Services (AWS) · DevOps · Attention to Detail · C# · Code Review