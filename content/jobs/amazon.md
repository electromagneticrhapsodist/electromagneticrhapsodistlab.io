---
title: Amazon
date: "February 2, 2025"
description: Amazon Employment
company: Amazon
role: Team/Technical Lead, Scrum Master and Systems Trainer
start_date: "October 2012"
end_date: "June 2016"
order: "2"
---

_Software Engineer_ : _October 2012 - June 2016_
Technical Team Lead for Full-Stack Development and Operations 
Led a team of five engineers providing 24x7 development and operations for third-party catalog data ingest, classification, and processing, managing billions of listing updates daily. 

Key Contributions: 
- Designed user experience and backend services using Amazon tools including Kafka, DynamoDB, EC2, and S3. 
- Played a pivotal role on a geographically diverse tiger team, spanning Seattle, Tempe, and Detroit, to develop a combined seller dashboard in just six weeks during a holiday code-freeze, launching the solution in January. 
- Transitioned full-stack software development and maintenance from Hyderabad, India, to Tempe, Arizona, restructuring domains and dividing responsibilities between front-end and back-end services. 
- Expanded the team from 4 to 18 members, leading hiring and onboarding efforts to establish a balanced team with 6 front-end and 12 back-end engineers. 

Seller Feedback and Buyer-to-Seller Messaging Initiatives 
- Acted as Team Manager during the transition of combined Seller Feedback and Buyer-to-Seller Messaging teams from Seattle to Tempe, supporting upper management through reorganization. 
- Migrated hybrid Perl/Mason pages to JavaScript/AJAX for Seller Feedback platforms, increasing user participation from 10% to 19% and reducing reactive software operations from 51% to 26%. 
- Transitioned Buyer-to-Seller Messaging infrastructure from C++ to Java Spring-based services, improving system reliability and maintainability. 

Infrastructure Modernization:
- Migrated services from internally managed hardware to Amazon EC2 instances, implementing fully automated CI/CD pipelines to enhance efficiency and reduce downtime. 

Demonstrated strong leadership, technical expertise, and process improvement capabilities, driving the successful delivery of critical projects and operational excellence for Amazon Marketplace.

Skills: Modernization · Messaging · Amazon Web Services (AWS) · DevOps · Attention to Detail · Code Review · Operations Administration · Systems Management · Commercial Operation · Data Centers