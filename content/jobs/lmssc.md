---
title: Lockheed Martin
date: "February 2, 2025"
description: Lockheed Martin Employment
company: Lockheed Martin Space Systems
role: Certified Principal Engineer/Software Systems Architect
start_date: "January 2004"
end_date: "October 2012"
order: "1"
---

_Certified Principal Engineer/Software Systems Architect_ : _January 2004 - October 2012_

End-to-End Solution Architect and Technical Lead 
Designed and implemented a scalable system to support a multi-Gb data rate increase, addressing next-generation ground processing requirements across diverse spacecraft payloads. 

Key Contributions:
- Developed and maintained ground processing applications for timing, attitude, ephemeris, and data-cleaning across 8 payloads spanning 7 spacecraft generations. 
- Certified critical data processing software for multiple analog and enhanced digital payloads supporting three launches, ensuring mission success. 
- Led a team of 8 engineers with varying experience levels (3-20 years), providing mentorship and technical direction. 

Software Development and Optimization: 
- Designed, developed, and tested station-keeping software for telemetry analysts, migrating 15K lines of C++ code to Java while adding 5K lines for 20 additional requirements. Completed this effort with 6 FTEs in just two months, meeting budget constraints. 
- Enhanced testing and tooling software for classified spacecraft programs, reducing the end-to-end testing lifecycle from 6 months to 4 weeks per payload. Recognized with an award by the LEO SIGINT Program Office for this achievement in Spring 2011. 

Proposal and Data Management Innovations: 
- Developed proprietary software for proposal management, supporting over 20 active pre-submittal activities and archiving hundreds of post-submittal proposals. This system streamlined Basis of Estimate Work Breakdown Structures and tasks for classified programs. 

Demonstrated expertise in scalable system design, software optimization, and team leadership, delivering solutions that improved operational efficiency and mission readiness for classified spacecraft programs.
