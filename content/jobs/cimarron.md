---
title: Cimarron
date: "February 2, 2025"
description: Current Employment
company: Cimarron
role: Systems Engineering Technical Advisor
start_date: "January 2020"
end_date: "January 2022"
order: "4"
---

_Systems Engineering Technical Advisor (SETA)_ : _January 2020 - January 2022_
Adapted to pandemic-induced schedule disruptions (Spring 2020 - Summer 2021) by implementing early/late shift rotations for SETA teams, ensuring seamless support for government contracts. Delivered technical guidance and program leadership to maintain productivity across multiple classified satellite programs, even amid shifting workforce schedules. 

Key Achievements
- Schedule Innovation: Introduced staggered shifts to mitigate knowledge gaps from a split workforce alternating weekly in-office and remote schedules, ensuring uninterrupted progress across programs. 
- Knowledge Continuity: Developed robust handoff notes and procedures, enabling late shifts to conduct research and provide technical depth for early shift meetings. 
- Classified Satellite Program Support: Maintained program momentum across lifecycle stages through resourceful scheduling and technical expertise. 

Technical Leadership
- Data Security Innovation: Co-authored “Integrating Security in Modern DataOps - Part I,” simplifying data security into a mnemonic “river” model, bridging physical, secure, and application layers. 
- Data Management Strategies: Guided government teams in developing unclassified solutions for seamless migration to classified systems with robust data management and classification protocols. 
- Pipeline Modernization: Transitioned programs from waterfall to CI/CD pipelines for four initiatives, integrating unclassified and classified environments. 

Agile Expertise 
- Scaled Agile Program Consultant (SAFe SPC): Facilitated government prioritization exercises, aligning multiple contracts with team capabilities using Agile Release Trains (ARTs) and Solution Trains. 
- Agile Training: Embedded Agile/Scrum consultant delivering project management training to contractor and subcontractor teams. 
- Cloud Engineering SME: Supported two NASA contract proposals as a Cloud Engineering and Scaled Agile Subject Matter Expert.

Skills: Modernization · Legacy Modernization · DevOps · Attention to Detail · C# · Code Review