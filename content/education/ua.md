---
title: University of Arizona
date: "April 19, 2024"
description: University of Arizona
company: University of Arizona
role: Bachelor of Science - Computer Science
start_date: "August 1998"
end_date: "December 2003"
order: "1"
---

_Bachelor of Science - Computer Science_ : _December 2003_

_Bachelor of Arts - Physics_ : _December 2003_

