Cari Copeland
179 Ambrosia Lane 
Martinsburg, WV 25405
cacopeland@protonmail.com
703.405.9038
April 24, 2024

National Radio Astronomy Observatory
1400 16th Street NW, Suite 730
Washington, DC, 20036
202.462.1676


Dear Hiring Manager,

I am writing to express my interest in the Software Engineer position with the Scientific Support and Archive development team at the National Radio Astronomy Observatory. The prospect of contributing to the advancement of radio astronomy and enhancing mission-critical toolsets resonates deeply with my passion for technology-driven exploration of the universe.

Grounded with dual bachelor's degrees in Physics and Computer Science from the University of Arizona, coupled with over two decades of hands-on engineering experience, I have crafted, delivered, and integrated sophisticated software and hardware solutions for classified government satellites and their ground stations. My expertise spans from software development for ephemeris orbit determination to spacecraft stationkeeping and signals data compression, complemented by extensive systems engineering integration and testing proficiency. These experiences uniquely position me to excel within the NRAO Ecosystem.

While my recent roles at Amazon, Intech, Cimarron, and Happy Money have enriched my toolkit with the latest development methodologies allowing me to grow my skills as an agile scrum master and coach, I have found my deepest fulfillment with my roles at General Dynamics Mission Payloads and Lockheed Martin Space Systems. At GD, as an Advanced Spacecraft Systems Architect, I orchestrated full lifecycle solution development and seamless integration efforts across diverse platforms. Similarly, my role at Lockheed Martin as a Certified Principal Engineer and Software Systems Architect involved architecting end-to-end solutions and leading teams to develop critical software systems for spacecraft payloads. Notably, I received accolades for pioneering next-generation analysis toolsets, operating on multi-GB real-time data, and ensuring the success of payloads and spacecraft operations across multiple launches.

I am intrigued by NRAO's commitment to scientific excellence and innovation for the Next Generation VLA.  I have included a copy of my most recent PDF resume, showcasing my ability to provide a condensed summary of my experience and skills focused on delivering a visually appealing set of diverse content in a single glance. I also have a digitially hosted version of my resume at https://electromagneticrhapsodist.gitlab.io to highlight the simplicity and reliability of a fully integrated CI/CD pipeline hosted by gitlab, with the focus on the ease of adding new content.  I am happy to provide access to the Gitlab Repo, should you like to see the organization and implementation of this pipeline.

Thank you for considering my application. I am enthusiastic about the opportunity to contribute to the development of the next generation of tools crucial to the NRAO's mission. I am available at your convenience for further discussion and look forward to the possibility of joining your team.

Respectfully,

Cari Copeland