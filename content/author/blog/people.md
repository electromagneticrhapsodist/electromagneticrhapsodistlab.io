---
title: People
---

[The Nerd Handbook](#rands-nerd)

[How To Rands](#rands-howto)

[Break Organization Dependencies with an "E Shaped Person")(#e-shaped)

[Maker's vs. Manager's](#maker-vs-manager)

[Storm to Perform](#storm-perform)

[Turn the Ship Around, Dave Marquet]{#turn-ship)

[rands-nerd]: https://randsinrepose.com/archives/the-nerd-handbook/
[rands-howto]: https://randsinrepose.com/archives/how-to-rands/
[e-shaped]: https://leadingagile.com/2017/02/e-shaped-person-staff/
[maker-vs-manager]: http://paulgraham.com/makersschedule.html
[storm-perform]: https://blog.trello.com/form-storm-norm-perform-stages-of-team-productivity
[turn-ship]: https://youtube.com/watch?v=lzJL8zX3EVk