---
title: SecDevOps
---

[Minimum Viable CD](#min-cd)

[SAFe DevOps](#safe-devops)

[Making Sense of MVP](#minvp)

[safe-devops]: https://scaledagileframework.com/devops/
[min-cd]: https://minimumcd.org
[minvp]: https://blog.crisp.se/2016/01/25/henrickkinberg/making-sense-of-mvp