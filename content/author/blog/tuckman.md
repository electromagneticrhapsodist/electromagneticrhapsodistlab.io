---
title: Form, Storm, Norm, Perform, Adjourn
---

All teams experience the [Tuckman model](https://blog.trello.com/form-storm-norm-perform-stages-of-team-productivity).  And all members of a team whether the team has just formed, or whether a new member is added or both have to continually go through the stages of Forming, Storming, Norming, and Performing when anything in their routine changes.  I'm fairly convinced that this happens to individuals too.

A routine however assumes that you've made it to the Norming Phase.  Not many teams do, especially when something (scope, member of team, lanuch, etc) changes.  Let's face it, our digital workd changes so rapidly, that our poor lizard brains can't really keep up.

