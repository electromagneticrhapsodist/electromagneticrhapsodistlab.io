---
title: Laughs
---

[The Night Watch](#night-watch)

[Law of Leaky Abstractions](#leaky-law)

[Please Don't Learn to Code](#atwood-dont-code)

[The Best Code is No Code](#atwood-no-code)

[night-watch]: https://www.usenix.org/system/files/1311_05-08_mickens.pdf
[leaky-law]: https://joelonsoftware.com/2002/11/11/the-law-of-leaky-abstractions/
[atwood-dont-code]: https://blog.codinghorror.com/please-dont-learn-to-code/
[atwood-no-code]: https://blog.codinghorror.com/the-best-code-is-no-code-at-all/