---
title: Rule #2 - Never Promise Anything You Don't Already Have
---

Development of *anything* always goes south.  People, by nature are overly optimistic about their abilities and how long things take.  Like now, I realized I started realizing this, because I thought I had more time but of coures, the fcoffee just finished brewing and life gets in the way.

The point is, done is often better than good so understanding this when setting expectations is imperative.

I often refer to the [Pareto Principle](#pareto) in this case most importantly that:

    20% of the input creates 80% of the result

but also that the last 20% of the effort also takes 80% of the time.

This is why it's hard to meet deliveries of unknown quantities of things of unknown scope.  Any work that is outside a well regulated and managed assembly line is hard to predict, that is why focusing on small batch sizes

    While increasing the processing rate (more efficient development and test practices, automation, etc.) is a consistent and common goal, the fastest method to reduce wait time is to reduce the queue length.

    (ref: [Principle #6 - Visualize and Limit WIP](#safe-wip))

and

    Overloading teams and ARTs with more work than can be reasonably accomplished is a common and pernicious problem.  Too much work in process (WIP) confuses priorities, causes frequent context switching, and increases overhead.  It overloads people, scatters focus on immediate tasks, reduces productivity and trhoughput, and increases wait times for new functionality.  Like a highwary at rush hour, there is simply no upside to having more work in a system than the system can handle.

    (ref: [Make Value Flow Without Interruptions](#safe-value-flow))
    
is so important.

---

One of the greatest values of an Agile team is that of being self-organizing.  By providing a [North Star](#north-star) or [Commander's Intent](#commanders-intent) you empower your teams to make their own decisions about how to best accomplish the work.

    Knowledge workers themselves are best placed to make decisions about how to perform their work.  --Peter Druker

One of my favorite lectures is that by Dave Marquet - referred to in Simon Sinek's [Leader's Eat Last](#leaders-eat-last)

[Turn the Ship Around](#turn-ship)

Leaders are to provide [Direction, Protection and Order](#dir-protect-order)

For those of us in the middle, we must manage both up understanding the business need and North Star and providing that guidance down so that we can get out of the way and Trust our teams to deliver.

[pareto]: https://betterexplained.com/articles/understanding-the-pareto-principle-the-8020-rule/
[safe-wip]: https://scaledagileframework.com/visualize-and-limit-wip-reduce-batch-sizes-and-manage-queue-lengths/
[safe-value-flow]: https://scaledagileframework.com/make-value-flow-without-interruptions/
[north-star]: https://productplan.com//blog/roadmap-themes-nort-star/
[commanders-intent]: https://hbr.org/2010/11/dont-play-golf-in-a-football-g
[leaders-eat-last]: https://shortform.com/summary/leaders-eat-last-summary-simon-sinek?
[turn-ship]: https://youtube.com/watch?v=lzJL8zX3EVk
[dir-protect-order]: https://youtube.com/watch?v=Dw1Pj4ZknWo


