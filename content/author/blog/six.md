---
title: Everything Takes 6 Weeks
---

# Week 1

I call the first week the honeymoon or delusional week.  Often the first week of a two-week sprint especially when starting _anything_ new.  You start out this week with bright eyes and big hopes knowing that you are going to do a great job.

# Week 2

Week 2 is the "Oh God, what have I committed myself to again?" - This is when reality starts to set in, and you think, oh boy this is definately going to be much harder than anticipated, because I agreed to too much and how in the world is this even going to work.  Coming at the end of a two-week sprint, you (and your team) can feel defeated, because you certainly didn't make the kind of progress you were hoping for when you started the climb at the beginning of week 1.

# Week 3

Week 3 is the "Everything is broken" week but you really just have barely scrateched the surface.  Here you think ok, if I can just get everyone to agree to a rescoping of the problem, then maybe we can get something accomplished by the end of the next sprint (week 4 in the case of two week sprints).  You are still delusional thinking you will accomplish everything you originally agreed to three weeks ago, but none-the-less you pull through.

# Week 4

Week 4 is recovery from everything being broken where you finally settle on what you really needed to do in Week 1.  You make a herculean push to get everything delivered, deployed, re-negotiated.  In the case of deploying something to production you will clearly believe you hit your need, but...

# Week 5

Week 5 is "No, really everything you thought was broken, is borken and oh its also several layers of dumpster fire[1]."  Here all you really are trying to do is hold it all together trying to get everyone on the same page and officially re-negotiate your agreements, minmal viable products, deliverables etc.  Only to find yourself at the end of week 5 headed into...

# Week 6

Week 6 I call "Wrong rock".  This is where you show the thing you made, supposedly with its polish and bug fixes to your "owner" or stakeholders.  This is when the finished thing doesn't look like any of the past negotiations/agreements/discussions but instead is some semblance of what you thought you wanted, what you were headed to deliver and what you actually needed.

***

[Cyclic](#cyclic) is the nature of our business delivering and [Scaling People](#scaling) that relies on technology to support it, in any fashion.  This is the shortest people cycle that I've been able to adequately replicate and burn through for a team to support them from [Storming to Norming](#storm-to-norm).
