---
title: Delivering at the Speed of Value
---

Wikis and Blogs are great, but at the speed of value, things become untenable and out-of-date fast and there really is just too much to know about too many things to be a master in all.  The number one skill I've acquired in my career was to look at the last edit date of any digital content.  If it was more than 3 months out of date, especially if it was related to a technical solution whether written inside a company our outside, it was probably wrong.  Best to create new, link to the old for historical purposes and move on.  Sometimes you can edit the content, bu that doesn't always make sense.  Talk to the last author, figure out how to leave what you stepped in better.

I consider myself a Jill of all Skills doer of stuff and things. I am a philosopher first, a scientist second, and an engineer third.  I consider myself a comb-shaped Engineer - but some of my teeth have both broken off and are shorter than others, ref [T-shape](#t-shape) and [E-shape](#e-shape) engineers.

I am an introvert and I too suffer from both the [Dunning-Kruger effect](https://www.britannica.com/science/Dunning-Kruger-effect) and [Impostor syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome) and I experience it daily.  This happens when I don't get enough time to perform [Deep Work](https://www.youtube.com/embed/gTaJhjQHcf8) and am on the [Manager vs. Maker Schedule](http://www.paulgraham.com/makersschedule.html)

I like the [Pomodoro Technique](https://www.themuse.com/advice/take-it-from-someone-who-hates-productivity-hacksthe-pomodoro-technique-actually-works); I learn best by doing[3](#nicho-eth); followed by reading how to do.  I tend to avoid YouTube except when it's "showing" me how to do something practical.

[#t-shape](https://jchyip.medium.com/why-t-shaped-people-e8706198e437)
[#e-shape](https://www.leadingagile.com/2017/02/e-shaped-person-staff/)
[#nico-eth]("For the things we have to learn before we can do them, we learn by doing them." Aristotle, Nichomachean Ethics, book II, verse 9)