---
title: Operating System
contact: TBD
date: TBD
---


#### contact me

#### man me

    Your nerd has control issues. Your nerd lives in a monospaced typeface world. Whereas everyone else is traipsing around picking dazzling fonts to describe their world, your nerd has carefully selected a monospace typeface, which he avidly uses to manipulate the world deftly via a command line interface while the rest fumble around with a mouse.

from [The Nerd Handbook](https://randsinrepose.com/archives/the-nerd-handbook/) - by [Rands In Repose](https://randsinrepose.com/)

#### uname -m

    Gen-X

Technically per most definitions some may say I'm a Milleinial, but in today's terms I can identify as anything I want, and I identify as a [Gen-X](https://www.britannica.com/topic/Generation-X)

#### uname -a

    INT[J|P]

I am an introvert, but I play an extrovert on TV.  I'm exhausted after too much peopling, and that includes the Zoom Burnout.

If I seem dis-compassionate it is likely because I have low energy.

If you know your [MBTI](myersbriggs.org/my-mbti-personality-type-mbti-basics), I'll talk to you about it for hours.

#### cal me

    Time is a construct.
    All techology is for my convienice.
    In general, most inquiries will be returned within 72 hours.




