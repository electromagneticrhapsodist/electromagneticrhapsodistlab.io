---
title: Decomposing Business Value
---

[SAFe Requirements Model](#safe-req)

[SAFe Epics](#safe-epic)

[Behavior Driven Development](#safe-bdd)

[Test Driven Development](#safe-tdd)

[Vizualize and Limit WIP](#safe-wip)

[safe-req]: https://scaledagileframework.com/safe-requirements-model/
[safe-epic]: https://scaledagileframework.com/epic/
[safe-bdd]: https://scaledagileframework.com/behavior-driven-development/
[safe-tdd]: https://scaledagileframework.com/test-driven-development/
[safe-wip]: https://scaledagileframework.com/vizualizes-and-limit-wip-reduce-batch-sizes-and-manage-queue-lengths/